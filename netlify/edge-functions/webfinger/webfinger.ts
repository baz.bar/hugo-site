import { Status} from "https://deno.land/std@0.136.0/http/http_status.ts";
import type { Context } from 'https://edge.netlify.com'

export default async (request: Request, context: Context) => {
    const source = "https://msz.baz.bar/.well-known/webfinger";
    const url = new URL(request.url);
    const resource = url.searchParams.get("resource");
    
    if (resource === null) {
	return context.json(
	    {
		error: "No 'resource' query parameter was provided",
	    },
	    {
		status: Status.BadRequest,
	    },
	);
    } else {
	const response = await fetch(`${source}?resource=${resource}`);

	if (response.ok) {
	    const data = await response.json();
	    
	    data.links.push({
		rel: "http://openid.net/specs/connect/1.0/issuer",
		href: "https://id.baz.bar/realms/cloud",
	    });
	    
	    return context.json(data);
	} else {
	    return context.json(
		{
		    error: "Resource not found",
		},
		{
		    status: Status.NotFound,
		},
	    );
	}
    }
}
