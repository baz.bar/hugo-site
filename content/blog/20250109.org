#+title: BÚÉK! and Spam.
#+date: 2025-01-09

I finally started getting spam. Well, phishing emails,
technically. From one specific source, some random VM probably
purchased for just the occasion. No matter, checking the headers, I
found DMARC would have successfully failed it. (Successfully failed?
That's a thing, right?)

# more

So I set about installing and configuring OpenDMARC. In the process, I
realized, despite the lack of obvious mention in the configuration
file, that Debian configures Postfix to run chroot by default. So I
also ended up going back to using a unix socket for OpenDKIM. Win,
win! I won't bore you with detailed step by step instructions. Suffice
it to say, I found a couple of articles and, per my usual fashion, I
cherry picked the bits I needed and changes the ones I didn't.

- [[https://petermolnar.net/article/howto-spf-dkim-dmarc-postfix/][Getting DKIM, DMARC[,] and SPF to Work with Postfix, OpenDKIM[,] and OpenDMARC]] by Peter Molnar
- [[https://www.linuxbabe.com/mail-server/opendmarc-postfix-ubuntu][Set Up OpenDMARC with Postfix on Ubuntu to Block Spam/Email Spoofing]] by Linux Babe

Yes, yes; I am hashtag pedantic about Oxford commas. Also, yes, I
happily sent phishing notices to the registrar and VM provider.
